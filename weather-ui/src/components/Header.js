import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => {
  return (
    <div className="flex pa1 justify-between nowrap orange">
      <div className="flex flex-fixed black">
        <div className="fw7 mr1">Weather service</div>
        <Link to="/addCity" className="ml1 no-underline black">
          create city
        </Link>
        <div className="ml1">|</div>
        <Link to="/deleteCity" className="ml1 no-underline black">
          delete city
        </Link>
        <div className="ml1">|</div>
        <Link
          to="/createScheduler"
          className="ml1 no-underline black"
        >
          create scheduler
        </Link>
        <div className="ml1">|</div>
        <Link
            to="/updateScheduler"
            className="ml1 no-underline black"
        >
          update scheduler
        </Link>
        <div className="ml1">|</div>
        <Link
            to="/deleteScheduler"
            className="ml1 no-underline black"
        >
          delete scheduler
        </Link>
      </div>
    </div>
  );
};

export default Header;

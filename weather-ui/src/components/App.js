import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Header from './Header';
import AddCity from './mutations/AddCity';
import DeleteCity from './mutations/DeleteCity';
import CreateScheduler from './mutations/CreateScheduler';
import UpdateScheduler from './mutations/UpdateScheduler';
import DeleteScheduler from './mutations/DeleteScheduler';

const App = () => (
  <div className="center w85">
    <Header />
    <div className="ph3 pv1 background-gray">
      <Switch>
        <Route exact path="/addCity" component={AddCity} />
        <Route exact path="/deleteCity" component={DeleteCity} />
        <Route exact path="/createScheduler" component={CreateScheduler} />
        <Route exact path="/updateScheduler" component={UpdateScheduler} />
        <Route exact path="/deleteScheduler" component={DeleteScheduler} />
      </Switch>
    </div>
  </div>
);

export default App;

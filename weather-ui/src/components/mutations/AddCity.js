import React, { useState } from 'react';
import { gql, useMutation } from '@apollo/client';

const ADD_CITY_MUTATION = gql`
  mutation AddCity($city: CityInput!) {
    addCity(city: $city) {
      name
    }
  }
`;

const AddCity = () => {
  const [formState, setFormState] = useState({
    name: ''
  });

  const [add_city] = useMutation(ADD_CITY_MUTATION, {
    variables: {
        city: {
            name: formState.name
        }
    },
    onCompleted: ({ addCity }) => {
      alert("City with name " + addCity.name + " added")
    }
  });
  return (
    <div>
      <input
          value={formState.name}
          onChange={(e) =>
              setFormState({
                ...formState,
                name: e.target.value
              })
          }
          type="text"
          placeholder="Your name"
      />




        <div className="flex mt3">
            <button
                className="pointer mr2 button"
                onClick={add_city}
            >
                Добавить город
            </button>
        </div>
    </div>
  );
};

export default AddCity;

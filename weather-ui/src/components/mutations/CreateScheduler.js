import React, { useState } from 'react';
import { gql, useMutation } from '@apollo/client';

const ADD_CITY_MUTATION = gql`
    mutation CreateScheduler($sheduler: SchedulerInput!) {
        addScheduler(schedulerRequest: $sheduler) {
            value
            messageError
        }
    }
`;

const UpdateScheduler = () => {
    const [formState, setFormState] = useState({
        name: ''
    });

    const [create_scheduler] = useMutation(ADD_CITY_MUTATION, {
        variables: {
            sheduler: {
                cityName: formState.cityName,
                cron: formState.cron
            }
        },
        onCompleted: ({ addScheduler }) => {
            alert(addScheduler.value === true ? "Scheduler is added" : "Sheduler is not added")
        }
    });
    return (
        <div>
            <input
                value={formState.cityName}
                onChange={(e) =>
                    setFormState({
                        ...formState,
                        cityName: e.target.value
                    })
                }
                type="text"
                placeholder="City name"
            />

            <input
                value={formState.cron}
                onChange={(e) =>
                    setFormState({
                        ...formState,
                        cron: e.target.value
                    })
                }
                type="text"
                placeholder="Cron expression"
            />

            <div className="flex mt3">
                <button
                    className="pointer mr2 button"
                    onClick={create_scheduler}
                >
                    Запланировать задачу
                </button>
            </div>
        </div>
    );
};

export default UpdateScheduler;

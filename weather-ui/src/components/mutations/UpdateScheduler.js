import React, { useState } from 'react';
import { gql, useMutation } from '@apollo/client';

const UPDATE_SCHEDULER_MUTATION = gql`
    mutation UpdateScheduler($sheduler: SchedulerInput!) {
        updateScheduler(schedulerRequest: $sheduler) {
            value
            messageError
        }
    }
`;

const CreateScheduler = () => {
    const [formState, setFormState] = useState({
        name: ''
    });

    const [create_scheduler] = useMutation(UPDATE_SCHEDULER_MUTATION, {
        variables: {
            sheduler: {
                cityName: formState.cityName,
                cron: formState.cron
            }
        },
        onCompleted: ({ updateScheduler }) => {
            alert(updateScheduler ? "Scheduler is updated" : "Scheduler is not updated")
        }
    });
    return (
        <div>
            <input
                value={formState.cityName}
                onChange={(e) =>
                    setFormState({
                        ...formState,
                        cityName: e.target.value
                    })
                }
                type="text"
                placeholder="City name"
            />

            <input
                value={formState.cron}
                onChange={(e) =>
                    setFormState({
                        ...formState,
                        cron: e.target.value
                    })
                }
                type="text"
                placeholder="Cron expression"
            />

            <div className="flex mt3">
                <button
                    className="pointer mr2 button"
                    onClick={create_scheduler}
                >
                    Обновить планировщик
                </button>
            </div>
        </div>
    );
};

export default CreateScheduler;

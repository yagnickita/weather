import React, { useState } from 'react';
import { gql, useMutation } from '@apollo/client';

const ADD_CITY_MUTATION = gql`
    mutation DeleteCity($city: CityInput!) {
        deleteCity(city: $city) {
            name
        }
    }
`;

const DeleteCity = () => {
    const [formState, setFormState] = useState({
        name: ''
    });

    const [delete_city] = useMutation(ADD_CITY_MUTATION, {
        variables: {
            city: {
                name: formState.name
            }
        },
        onCompleted: ({ deleteCity }) => {
            alert("City with name " + deleteCity.name + " is deleted")
        }
    });
    return (
        <div>
            <input
                value={formState.name}
                onChange={(e) =>
                    setFormState({
                        ...formState,
                        name: e.target.value
                    })
                }
                type="text"
                placeholder="Your name"
            />




            <div className="flex mt3">
                <button
                    className="pointer mr2 button"
                    onClick={delete_city}
                >
                    Удалить город
                </button>
            </div>
        </div>
    );
};

export default DeleteCity;

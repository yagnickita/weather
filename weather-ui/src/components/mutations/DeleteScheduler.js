import React, { useState } from 'react';
import { gql, useMutation } from '@apollo/client';

const DELETE_SCHEDULER_MUTATION = gql`
    mutation DeleteScheduler($sheduler: SchedulerInput!) {
        deleteScheduler(schedulerRequest: $sheduler) {
            value
            messageError
        }
    }
`;

const DeleteScheduler = () => {
    const [formState, setFormState] = useState({
        name: ''
    });

    const [delete_scheduler] = useMutation(DELETE_SCHEDULER_MUTATION, {
        variables: {
            sheduler: {
                cityName: formState.cityName,
            }
        },
        onCompleted: ({ deleteScheduler }) => {
            alert(deleteScheduler ? "Scheduler is deleted" : "Scheduler is not deleted")
        }
    });
    return (
        <div>
            <input
                value={formState.cityName}
                onChange={(e) =>
                    setFormState({
                        ...formState,
                        cityName: e.target.value
                    })
                }
                type="text"
                placeholder="City name"
            />

            <div className="flex mt3">
                <button
                    className="pointer mr2 button"
                    onClick={delete_scheduler}
                >
                    Удалить задачу
                </button>
            </div>
        </div>
    );
};

export default DeleteScheduler;

package ru.onlineservices.weatherservice.quartz;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;
import ru.onlineservices.weatherservice.quartz.wrapper.CronTriggerFactoryBeanWrapper;
import ru.onlineservices.weatherservice.quartz.wrapper.SimpleTriggerFactoryBeanWrapper;

import java.text.ParseException;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class JobScheduleCreatorTest {

    @InjectMocks
    JobScheduleCreator jobScheduleCreator;

    @Mock
    CronTriggerFactoryBeanWrapper cronTriggerFactoryBeanWrapper;

    @Mock
    SimpleTriggerFactoryBeanWrapper simpleTriggerFactoryBeanWrapper;

    @Captor
    ArgumentCaptor<String> setName;

    @Captor
    ArgumentCaptor<Date> setStartTime;

    @Captor
    ArgumentCaptor<Long> setRepeatInterval;

    @Captor
    ArgumentCaptor<Integer> setMisFireInstruction;

    @Captor
    ArgumentCaptor<Integer> setRepeatCount;

    @Captor
    ArgumentCaptor<String> setCronExpression;

    @Test
    @DisplayName("Check that create cron trigger")
    void whenCreateCronTriggerTest() throws ParseException {
        CronTriggerFactoryBean factoryBean = Mockito.mock(CronTriggerFactoryBean.class);
        when(cronTriggerFactoryBeanWrapper.initCronTriggerFactoryBeanWrapper()).thenReturn(factoryBean);
        Mockito.doNothing().when(factoryBean).afterPropertiesSet();

        jobScheduleCreator.createCronTrigger("test", new Date(), "", 1);

        Mockito.verify(factoryBean).setName(setName.capture());
        Mockito.verify(factoryBean).setStartTime(setStartTime.capture());
        Mockito.verify(factoryBean).setCronExpression(setCronExpression.capture());
        Mockito.verify(factoryBean).setMisfireInstruction(setMisFireInstruction.capture());

        assertNotNull(setName);
        assertNotNull(setStartTime);
        assertNotNull(setCronExpression);
        assertNotNull(setMisFireInstruction);
    }

    @Test
    @DisplayName("Check that create simple trigger")
    void createSimpleTriggerTest() {
        SimpleTriggerFactoryBean factoryBean = Mockito.mock(SimpleTriggerFactoryBean.class);
        when(simpleTriggerFactoryBeanWrapper.initSimpleTriggerFactoryBean()).thenReturn(factoryBean);
        Mockito.doNothing().when(factoryBean).afterPropertiesSet();

        jobScheduleCreator.createSimpleTrigger("test", new Date(), 100L, 1);

        Mockito.verify(factoryBean).setName(setName.capture());
        Mockito.verify(factoryBean).setStartTime(setStartTime.capture());
        Mockito.verify(factoryBean).setRepeatInterval(setRepeatInterval.capture());
        Mockito.verify(factoryBean).setRepeatCount(setRepeatCount.capture());
        Mockito.verify(factoryBean).setMisfireInstruction(setMisFireInstruction.capture());

        assertNotNull(setName);
        assertNotNull(setStartTime);
        assertNotNull(setRepeatInterval);
        assertNotNull(setRepeatCount);
        assertNotNull(setMisFireInstruction);
    }
}
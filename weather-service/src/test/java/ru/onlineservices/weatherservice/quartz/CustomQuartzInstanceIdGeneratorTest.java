package ru.onlineservices.weatherservice.quartz;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.quartz.SchedulerException;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
public class CustomQuartzInstanceIdGeneratorTest {

    @InjectMocks
    CustomQuartzInstanceIdGenerator customQuartzInstanceIdGenerator;

    @Test
    @DisplayName("Generate instance id")
    public void generateInstanceIdTest() throws SchedulerException {
        assertNotNull(customQuartzInstanceIdGenerator.generateInstanceId());
    }
}
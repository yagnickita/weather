package ru.onlineservices.weatherservice.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.quartz.*;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import ru.onlineservices.weatherservice.entity.SchedulerJobInfo;
import ru.onlineservices.weatherservice.entity.SchedulerRequest;
import ru.onlineservices.weatherservice.quartz.JobScheduleCreator;
import ru.onlineservices.weatherservice.repo.SchedulerRepository;
import ru.onlineservices.weatherservice.service.impl.SchedulerServiceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class SchedulerServiceTest {

    @InjectMocks
    SchedulerServiceImpl schedulerService;

    @Mock
    SchedulerRepository schedulerRepository;

    @Mock
    SchedulerFactoryBean schedulerFactoryBean;

    @Mock
    JobScheduleCreator scheduleCreator;

    private CronTrigger cronTrigger;
    private Scheduler scheduler;

    @BeforeEach
    public void setUp() {
        cronTrigger = Mockito.mock(CronTrigger.class);
        scheduler = Mockito.mock(Scheduler.class);
    }

    @Test
    @DisplayName("Check that add schedulers started")
    public void whenStartedAllSchedulersTest() {
        Scheduler scheduler = Mockito.mock(Scheduler.class);
        List<SchedulerJobInfo> list = new ArrayList<>();
//        SchedulerJobInfo jobInfo = new SchedulerJobInfo();
//        jobInfo.setJobName("test");
//        jobInfo.setCronJob(true);
//        jobInfo.setCronExpression("0 0/1 * ? * *");
//        list.add(jobInfo);

        when(schedulerRepository.findAll()).thenReturn(list);
        when(schedulerFactoryBean.getScheduler()).thenReturn(scheduler);

        schedulerService.startAllSchedulers();

        verify(schedulerRepository, times(1)).findAll();
        verify(schedulerFactoryBean, times(1)).getScheduler();

    }

    @Test
    @DisplayName("Check that new job is create")
    public void whenCreateNewJobWithSchedulerTest() throws SchedulerException {
        JobDetail jobDetail = Mockito.mock(JobDetail.class);
        SchedulerRequest schedulerRequest = new SchedulerRequest();
        schedulerRequest.setCityName("test");
        schedulerRequest.setCron("0 0/1 * ? * *");

        when(schedulerFactoryBean.getScheduler()).thenReturn(scheduler);
        when(scheduler.checkExists(any(JobKey.class))).thenReturn(false);
        when(scheduleCreator.createJob(any(), eq(false), any(), eq("test"), eq(null))).thenReturn(jobDetail);
        when(scheduleCreator.createCronTrigger(anyString(), any(), eq("0 0/1 * ? * *"), eq(1))).thenReturn(cronTrigger);
        when(scheduler.scheduleJob(eq(jobDetail), eq(cronTrigger))).thenReturn(new Date());
//        when(schedulerRepository.save(any())).thenReturn(new SchedulerJobInfo());

        schedulerService.scheduleNewJob(schedulerRequest);
    }

    @Test
    @DisplayName("Check that job is updated")
    public void whenUpdateJobWithSchedulerTest() throws SchedulerException {
//        SchedulerRequest schedulerRequest = new SchedulerRequest();
//        schedulerRequest.setCityName("test");
//        schedulerRequest.setCron("0 0/1 * ? * *");
//        SchedulerJobInfo jobInfo = new SchedulerJobInfo();
//        jobInfo.setJobName("test");
//        jobInfo.setCronJob(true);
//
//        when(scheduleCreator.createCronTrigger(anyString(), any(), eq("0 0/1 * ? * *"), eq(1))).thenReturn(cronTrigger);
//        when(schedulerRepository.findByJobName(anyString())).thenReturn(jobInfo);
//        when(schedulerFactoryBean.getScheduler()).thenReturn(scheduler);
//        when(scheduler.rescheduleJob(any(), any())).thenReturn(new Date());
//
//        assertTrue(schedulerService.updateScheduleJob(schedulerRequest).getValue());
    }

    @Test
    @DisplayName("Check that job is deleted")
    public void whenDeleteJobWithSchedulerTest() throws SchedulerException {
        SchedulerRequest schedulerRequest = new SchedulerRequest();
        schedulerRequest.setCityName("test");

        when(schedulerFactoryBean.getScheduler()).thenReturn(scheduler);
        when(scheduler.deleteJob(new JobKey("test"))).thenReturn(true);

        schedulerService.deleteJob(schedulerRequest);

        verify(schedulerRepository, times(1)).deleteByJobName("test");
        verify(schedulerFactoryBean, times(1)).getScheduler();
    }
}
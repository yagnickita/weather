package ru.onlineservices.weatherservice.service;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.onlineservices.weatherservice.repo.CityRepository;
import ru.onlineservices.weatherservice.repo.WeatherRepository;
import ru.onlineservices.weatherservice.service.impl.WeatherServiceImpl;

@ExtendWith(MockitoExtension.class)
public class WeatherServiceTest {

    @InjectMocks
    WeatherServiceImpl weatherService;

    @Mock
    CityRepository cityRepository;

    @Mock
    WeatherRepository weatherRepository;

//    @Test
//    @DisplayName("Check that weather and city init when weather updated")
//    public void whenUpdateWeatherTest() throws WeatherNotFoundException {
//        City city = new City();
//        Weather weather = new Weather();
//        weather.setId(1L);
//        city.setName("test");
//        city.setWeather(weather);
//        weather.setCity(city);
//        when(cityRepository.findByName(anyString())).thenReturn(city);
//        when(weatherRepository.findById(1L)).thenReturn(Optional.of(weather));
//
//        weatherService.updateWeather("test", 10);
//
//        verify(cityRepository, times(1)).findByName("test");
//        verify(weatherRepository, times(1)).findById(1L);
//        verify(weatherRepository, times(1)).save(weather);
//    }
}
package ru.onlineservices.weatherservice.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.onlineservices.weatherservice.entity.City;
import ru.onlineservices.weatherservice.entity.Weather;
import ru.onlineservices.weatherservice.repo.CityRepository;
import ru.onlineservices.weatherservice.repo.WeatherRepository;
import ru.onlineservices.weatherservice.service.impl.CityServiceImpl;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CityServiceTest {

    @InjectMocks
    private CityServiceImpl cityService;

    @Mock
    CityRepository cityRepository;

    @Mock
    SchedulerService schedulerService;

    @Mock
    WeatherRepository weatherRepository;

    @Test
    @DisplayName("Check that new city added")
    public void whenCityAddedToRepository() {
        City city = new City();
        Weather weather = new Weather();
        city.setName("test");
        when(weatherRepository.save(any())).thenReturn(weather);
        cityService.addCity(city);

        verify(weatherRepository, times(1)).save(any());
        verify(cityRepository, times(1)).save(any());
    }

//    @Test
//    @DisplayName("Check that new city deleted")
//    public void whenCityDeletedFromRepository() {
//        cityService.deleteCity(anyString());
//
//        verify(schedulerService, times(1)).deleteJobByName(anyString());
//        verify(cityRepository, times(1)).deleteByName(anyString());
//    }


//    @Test
//    @DisplayName("Check repository get all cities")
//    public void whenGetAllCitiesFromRepository() {
//        List<City> cities = new ArrayList<>();
//        cities.add(new City());
//
//        when(cityRepository.findAll()).thenReturn(cities);
//        cityService.deleteCity(anyString());
//        assertEquals(cities, cityService.getAllCities());
//
//        verify(cityRepository, times(1)).findAll();
//    }
}
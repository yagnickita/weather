package ru.onlineservices.weatherservice.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.onlineservices.weatherservice.entity.City;

@Repository
public interface CityRepository extends JpaRepository<City, String> {
}

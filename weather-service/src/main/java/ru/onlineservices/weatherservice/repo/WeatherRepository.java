package ru.onlineservices.weatherservice.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.onlineservices.weatherservice.entity.Weather;

@Repository
public interface WeatherRepository extends JpaRepository<Weather, Long> {

}

package ru.onlineservices.weatherservice.graphql;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import lombok.RequiredArgsConstructor;
import ru.onlineservices.weatherservice.entity.City;
import ru.onlineservices.weatherservice.entity.CityResponse;
import ru.onlineservices.weatherservice.exception.CityNotFoundException;
import ru.onlineservices.weatherservice.service.CityService;

@RequiredArgsConstructor
public class Query implements GraphQLQueryResolver {
    private final CityService cityService;

    public CityResponse city(City city) throws CityNotFoundException {
        City cityFound = cityService.getCity(city.getName());
        return new CityResponse(cityFound.getName(), true);
    }
}

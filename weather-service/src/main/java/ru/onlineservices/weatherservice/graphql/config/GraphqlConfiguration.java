package ru.onlineservices.weatherservice.graphql.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.onlineservices.weatherservice.graphql.Mutation;
import ru.onlineservices.weatherservice.graphql.Query;
import ru.onlineservices.weatherservice.service.CityService;
import ru.onlineservices.weatherservice.service.SchedulerService;

@Configuration
public class GraphqlConfiguration {

    @Bean
    public Mutation mutation(CityService cityService, SchedulerService schedulerService) {
        return new Mutation(cityService, schedulerService);
    }

    @Bean
    public Query query(CityService cityService) {
        return new Query(cityService);
    }
}

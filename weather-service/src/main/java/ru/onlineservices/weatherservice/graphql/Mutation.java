package ru.onlineservices.weatherservice.graphql;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import lombok.RequiredArgsConstructor;
import ru.onlineservices.weatherservice.entity.City;
import ru.onlineservices.weatherservice.entity.CityResponse;
import ru.onlineservices.weatherservice.entity.SchedulerRequest;
import ru.onlineservices.weatherservice.entity.Status;
import ru.onlineservices.weatherservice.service.CityService;
import ru.onlineservices.weatherservice.service.SchedulerService;

@RequiredArgsConstructor
public class Mutation implements GraphQLMutationResolver {

    private final CityService cityService;
    private final SchedulerService schedulerService;

    public CityResponse addCity(City city) {
        boolean status = cityService.addCity(city);
        return new CityResponse(city.getName(), status);
    }

    public CityResponse deleteCity(City city) {
        boolean status = cityService.deleteCity(city.getName());
        return new CityResponse(city.getName(), status);
    }

    public Status addScheduler(SchedulerRequest schedulerRequest) {
        return schedulerService.scheduleNewJob(schedulerRequest);
    }

    public Status updateScheduler(SchedulerRequest schedulerRequest) {
        return schedulerService.updateScheduleJob(schedulerRequest);
    }

    public Status deleteScheduler(SchedulerRequest schedulerRequest) {
        return schedulerService.deleteJob(schedulerRequest);
    }
}

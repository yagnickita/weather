package ru.onlineservices.weatherservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import ru.onlineservices.weatherservice.graphql.config.GraphqlConfiguration;

@SpringBootApplication
@Import(GraphqlConfiguration.class)
public class WeatherServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeatherServiceApplication.class, args);
	}

}

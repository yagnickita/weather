package ru.onlineservices.weatherservice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Status {
    private Boolean value;
    private String messageError;
}

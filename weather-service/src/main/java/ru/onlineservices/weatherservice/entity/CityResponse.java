package ru.onlineservices.weatherservice.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CityResponse {
    private String name;
    private Boolean status;
}

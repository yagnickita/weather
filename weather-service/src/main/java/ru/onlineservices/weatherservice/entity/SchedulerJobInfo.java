package ru.onlineservices.weatherservice.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Data
@Builder
@Table(name = "scheduler_job_info")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class SchedulerJobInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String jobName;

    private String jobGroup;

    private String jobClass;

    private String cronExpression;

    private Long repeatTime;

    private Boolean isCronJob;
}
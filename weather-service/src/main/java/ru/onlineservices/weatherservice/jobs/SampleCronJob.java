package ru.onlineservices.weatherservice.jobs;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ru.onlineservices.weatherservice.service.WeatherService;

import java.util.Map;

@Slf4j
@DisallowConcurrentExecution
@Component
@RequiredArgsConstructor
public class SampleCronJob extends QuartzJobBean {

    private final WeatherService weatherService;
    private final RestTemplate restTemplate;

    private static final String GET_WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather";

    /**
     * Send request for openweather
     * appid - created for tech user because time for active token about 3 hours
     * in real prod app need used {spring.appid} @Value
     *
     * @param context Context for job execute
     */
    @Override
    @SneakyThrows
    protected void executeInternal(JobExecutionContext context) {
        String cityName = context.getJobDetail().getKey().getName();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(GET_WEATHER_URL)
                .queryParam("appid", "3e3d98e514bc5576bee79af25833374a")
                .queryParam("q", cityName)
                .queryParam("units", "metric");

        HttpEntity<?> entity = new HttpEntity<>(headers);

        HttpEntity<Map> response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                entity,
                Map.class);
        Double temp = (Double) ((Map) response.getBody()
                .get("main"))
                .get("temp");
        Long roundTemp = Math.round(temp);
        weatherService.updateWeather(cityName, roundTemp);
    }
}

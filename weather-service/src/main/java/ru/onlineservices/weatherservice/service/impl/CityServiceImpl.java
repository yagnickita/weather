package ru.onlineservices.weatherservice.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.onlineservices.weatherservice.entity.City;
import ru.onlineservices.weatherservice.entity.Weather;
import ru.onlineservices.weatherservice.exception.CityNotFoundException;
import ru.onlineservices.weatherservice.repo.CityRepository;
import ru.onlineservices.weatherservice.repo.WeatherRepository;
import ru.onlineservices.weatherservice.service.CityService;
import ru.onlineservices.weatherservice.service.SchedulerService;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CityServiceImpl implements CityService {

    private final CityRepository cityRepository;
    private final WeatherRepository weatherRepository;
    private final SchedulerService schedulerService;

    /**
     * Delete city from repository
     *
     * @param cityName City name for delete city
     */
    @Override
    @Transactional
    public boolean deleteCity(String cityName) {
        schedulerService.deleteJobByName(cityName);
        Optional<City> cityOptional = cityRepository.findById(cityName);
        if (cityOptional.isPresent()) {
            cityRepository.deleteById(cityName);
            return true;
        }
        return false;
    }

    /**
     * Add city to database city
     *
     * @param city City for adding database
     */
    @Override
    @Transactional
    public boolean addCity(City city) {
        Optional<City> optionalCity = cityRepository.findById(city.getName());
        if (optionalCity.isEmpty()) {
            Weather weather = new Weather();
            weather.setCity(city);
            city.setWeather(weatherRepository.save(weather));
            cityRepository.save(city);
            return true;
        }
        return false;
    }

    /**
     * Get city from repository
     *
     * @return City from database
     */
    @Override
    public City getCity(String cityName) throws CityNotFoundException {
        return cityRepository.findById(cityName)
                .orElseThrow(() -> new CityNotFoundException(cityName));
    }
}

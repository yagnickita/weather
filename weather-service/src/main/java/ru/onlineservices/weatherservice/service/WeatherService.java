package ru.onlineservices.weatherservice.service;

public interface WeatherService {
    void updateWeather(String cityName, Long roundTemp);
}

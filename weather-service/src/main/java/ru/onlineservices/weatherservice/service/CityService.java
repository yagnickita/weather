package ru.onlineservices.weatherservice.service;


import ru.onlineservices.weatherservice.entity.City;
import ru.onlineservices.weatherservice.exception.CityNotFoundException;

public interface CityService {
    boolean deleteCity(String name);
    boolean addCity(City city);
    City getCity(String cityName) throws CityNotFoundException;
}

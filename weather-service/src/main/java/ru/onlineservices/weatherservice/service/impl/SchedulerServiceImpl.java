package ru.onlineservices.weatherservice.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.onlineservices.weatherservice.entity.SchedulerJobInfo;
import ru.onlineservices.weatherservice.entity.SchedulerRequest;
import ru.onlineservices.weatherservice.entity.Status;
import ru.onlineservices.weatherservice.jobs.SampleCronJob;
import ru.onlineservices.weatherservice.quartz.JobScheduleCreator;
import ru.onlineservices.weatherservice.repo.SchedulerRepository;
import ru.onlineservices.weatherservice.service.SchedulerService;

import java.util.Date;
import java.util.List;

@Slf4j
@Transactional
@Service
@RequiredArgsConstructor
public class SchedulerServiceImpl implements SchedulerService {

    private final SchedulerFactoryBean schedulerFactoryBean;
    private final SchedulerRepository schedulerRepository;
    private final ApplicationContext context;
    private final JobScheduleCreator scheduleCreator;

    public static final Class<? extends QuartzJobBean> handlerClassName = SampleCronJob.class;

    /**
     * Start all schedulers when start app
     */
    @Override
    public void startAllSchedulers() {
        List<SchedulerJobInfo> jobInfoList = schedulerRepository.findAll();
            Scheduler scheduler = schedulerFactoryBean.getScheduler();
            jobInfoList.forEach(jobInfo -> {
                try {
                    JobDetail jobDetail = JobBuilder.newJob(handlerClassName)
                            .withIdentity(jobInfo.getJobName()).build();
                    // check, that scheduler with job key not exists
                    // than create job and trigger
                    if (!scheduler.checkExists(jobDetail.getKey())) {
                        scheduleJob(jobInfo, scheduler);
                    }
                } catch (SchedulerException e) {
                    log.error(e.getMessage(), e);
                }
            });
    }

    /**
     * Create new scheduled job
     *
     * @param schedulerRequest Request for create new job
     * @return Status job created
     */
    @Override
    public Status scheduleNewJob(SchedulerRequest schedulerRequest) {
        try {
            SchedulerJobInfo jobInfo = SchedulerJobInfo.builder()
                    .cronExpression(schedulerRequest.getCron())
                    .isCronJob(true)
                    .jobClass(handlerClassName.getName())
                    .jobGroup(null)
                    .jobName(schedulerRequest.getCityName())
                    .build();

            Scheduler scheduler = schedulerFactoryBean.getScheduler();
            JobDetail jobDetail = JobBuilder.newJob(handlerClassName)
                    .withIdentity(jobInfo.getJobName()).build();

            if (!scheduler.checkExists(jobDetail.getKey())) {
                scheduleJob(jobInfo, scheduler);
                schedulerRepository.save(jobInfo);
                return new Status(true, null);
            } else {
                log.error("Job already exist");
                return new Status(false, "Job already exist");
            }
        } catch (SchedulerException e) {
            log.error(e.getMessage(), e);
        }
        return new Status(false, "Undefined error");
    }

    private boolean isCronJob(SchedulerJobInfo jobInfo) {
        // Check, that flag isCronJob = true and cron expression is valid
        return jobInfo.getIsCronJob() && CronExpression.isValidExpression(jobInfo.getCronExpression());
    }

    private void scheduleJob(SchedulerJobInfo jobInfo, Scheduler scheduler) throws SchedulerException {
        JobDetail jobDetail = scheduleCreator.createJob(handlerClassName, false, context, jobInfo.getJobName(), jobInfo.getJobGroup());
        Trigger trigger = createTrigger(jobInfo, isCronJob(jobInfo));
        scheduler.scheduleJob(jobDetail, trigger);
    }

    /**
     * Update scheduled job
     *
     * @param schedulerRequest Request for update job
     * @return Status job updated
     */
    @Override
    public Status updateScheduleJob(SchedulerRequest schedulerRequest) {
        SchedulerJobInfo jobInfo = schedulerRepository.findByJobName(schedulerRequest.getCityName());
        if (jobInfo == null) {
            log.info("Job not found in Scheduler repository");
            return new Status(false, "Job not found");
        }
        jobInfo.setCronExpression(schedulerRequest.getCron());
        Trigger trigger = createTrigger(jobInfo, jobInfo.getIsCronJob());
        try {
            schedulerFactoryBean.getScheduler().rescheduleJob(TriggerKey.triggerKey(jobInfo.getJobName()), trigger);
            schedulerRepository.save(jobInfo);
            return new Status(true, null);
        } catch (SchedulerException e) {
            log.error(e.getMessage(), e);
            return new Status(false, "Job scheduler not updated");
        }
    }

    private Trigger createTrigger(SchedulerJobInfo jobInfo, boolean cronJob) {
        return cronJob ? scheduleCreator.createCronTrigger(jobInfo.getJobName(), new Date(), jobInfo.getCronExpression(),
                SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW)
                       : scheduleCreator.createSimpleTrigger(jobInfo.getJobName(), new Date(), jobInfo.getRepeatTime(),
                SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
    }

    /**
     * Delete job by name
     *
     * @param jobName Job name for search repo when delete
     * @return Is job removed
     */
    @Override
    public Status deleteJobByName(String jobName) {
        SchedulerRequest schedulerRequest = new SchedulerRequest();
        schedulerRequest.setCityName(jobName);
        return deleteJob(schedulerRequest);
    }

    /**
     * Remove job
     *
     * @param schedulerRequest request for job deleted
     * @return Status remove job
     */
    @Override
    public Status deleteJob(SchedulerRequest schedulerRequest) {
        try {
            String cityName = schedulerRequest.getCityName();
            schedulerRepository.deleteByJobName(cityName);
            return new Status(schedulerFactoryBean.getScheduler().deleteJob(new JobKey(cityName)), null);
        } catch (SchedulerException e) {
            log.error("Failed to delete for job - {}", schedulerRequest.getCityName(), e);
            return new Status(false, "Failed delete");
        }
    }
}

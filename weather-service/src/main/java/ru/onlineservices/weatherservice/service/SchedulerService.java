package ru.onlineservices.weatherservice.service;

import ru.onlineservices.weatherservice.entity.SchedulerRequest;
import ru.onlineservices.weatherservice.entity.Status;

public interface SchedulerService {

    void startAllSchedulers();

    Status scheduleNewJob(SchedulerRequest schedulerRequest);

    Status updateScheduleJob(SchedulerRequest schedulerRequest);

    Status deleteJob(SchedulerRequest schedulerRequest);

    Status deleteJobByName(String jobName);

}

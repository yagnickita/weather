package ru.onlineservices.weatherservice.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.onlineservices.weatherservice.repo.CityRepository;
import ru.onlineservices.weatherservice.repo.WeatherRepository;
import ru.onlineservices.weatherservice.service.WeatherService;

@Service
@Slf4j
@RequiredArgsConstructor
public class WeatherServiceImpl implements WeatherService {

    private final WeatherRepository weatherRepository;
    private final CityRepository cityRepository;

    /**
     * Update weather info
     *
     * @param cityName  City for search in repo
     * @param roundTemp Temp for update
     */
    @Transactional
    @Override
    public void updateWeather(String cityName, Long roundTemp) {
        cityRepository.findById(cityName)
                .flatMap(city -> weatherRepository.findById(city.getWeather().getId()))
                .ifPresent(weather -> {
                    weather.setTemperature(roundTemp);
                    weatherRepository.save(weather);
                    log.info("Weather updated for city {}", cityName);
                });
    }
}

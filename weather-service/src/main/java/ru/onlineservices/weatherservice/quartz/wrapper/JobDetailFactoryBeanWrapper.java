package ru.onlineservices.weatherservice.quartz.wrapper;

import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.stereotype.Service;

@Service
public class JobDetailFactoryBeanWrapper {
    /**
     * Wrapper for JobDetailFactoryBean
     *
     * @return JobDetailFactoryBean
     */
    public JobDetailFactoryBean initJobDetailFactoryBean() {
        return new JobDetailFactoryBean();
    }
}

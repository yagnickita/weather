package ru.onlineservices.weatherservice.quartz;

import org.quartz.SchedulerException;
import org.quartz.spi.InstanceIdGenerator;

import java.util.UUID;

public class CustomQuartzInstanceIdGenerator implements InstanceIdGenerator {

    /**
     * Generate uniq instance id
     *
     * @return Random uniq instance id
     * @throws SchedulerException When job can't generate instance id
     */
    @Override
    public String generateInstanceId() throws SchedulerException {
        try {
            return UUID.randomUUID().toString();
        } catch (Exception ex) {
            throw new SchedulerException("Couldn't generate UUID!", ex);
        }
    }

}

package ru.onlineservices.weatherservice.quartz.wrapper;

import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.stereotype.Service;

@Service
public class CronTriggerFactoryBeanWrapper {
    /**
     * Wrapper forCronTriggerFactoryBean
     *
     * @return CronTriggerFactoryBean
     */
    public CronTriggerFactoryBean initCronTriggerFactoryBeanWrapper() {
        return new CronTriggerFactoryBean();
    }
}

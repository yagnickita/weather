package ru.onlineservices.weatherservice.quartz.wrapper;

import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;
import org.springframework.stereotype.Service;

@Service
public class SimpleTriggerFactoryBeanWrapper {

    /**
     * Wrapper for SimpleTriggerFactoryBean
     *
     * @return SimpleTriggerFactoryBean
     */
    public SimpleTriggerFactoryBean initSimpleTriggerFactoryBean() {
        return new SimpleTriggerFactoryBean();
    }

}

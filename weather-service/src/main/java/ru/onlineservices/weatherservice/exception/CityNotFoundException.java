package ru.onlineservices.weatherservice.exception;

import javassist.NotFoundException;

public class CityNotFoundException extends NotFoundException {
    public CityNotFoundException(String cityName) {
        super("City with name " + cityName + " not found");
    }
}
